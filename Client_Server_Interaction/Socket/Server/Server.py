import socket
import threading
import time
import wave

CHANNELS = 1
RATE = 44100
CHUNK = 1024
global_udp_port = None
stop_server = False
client_timeout = 1


def handle_client(conn, addr):
    print(f"Connected TO TCP-server by {addr}")
    try:
        conn.sendall(str(global_udp_port).encode('utf-8'))
    finally:
        conn.close()


def record_data(client_addr, data, file_dict, last_seen):
    if client_addr not in file_dict:
        wf = wave.open(f'output_{client_addr[0]}_{client_addr[1]}.wav', 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(2)
        wf.setframerate(RATE)
        file_dict[client_addr] = wf
        print(f"File created for {client_addr}")

    file_dict[client_addr].writeframes(data)
    last_seen[client_addr] = time.time()


def start_udp_server():
    global global_udp_port, stop_server

    udp_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_server.bind(('127.0.0.1', 0))
    udp_port = udp_server.getsockname()[1]
    print(f"UDP server listening on port {udp_port}")

    global_udp_port = udp_port
    client_files = {}
    last_seen = {}
    monitor_thread = threading.Thread(target=monitor_clients, args=(last_seen, client_files))
    monitor_thread.start()

    try:
        while not stop_server:
            try:
                udp_server.settimeout(1)
                data, client_addr = udp_server.recvfrom(CHUNK * CHANNELS * 2)
            except socket.timeout:
                continue

            if data:
                record_data(client_addr, data, client_files, last_seen)
                print(f"Received {len(data)} bytes of data from {client_addr}")

    except KeyboardInterrupt:
        print("Terminating server...")
    finally:
        print("Closing audio files and UDP server...")
        for wf in client_files.values():
            wf.close()
        udp_server.close()


def start_tcp_server():
    global stop_server

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('127.0.0.1', 8888))
    s.listen()
    s.settimeout(1)

    print("TCP server is listening...")

    try:
        while not stop_server:
            try:
                conn, addr = s.accept()
            except socket.timeout:
                continue

            client_thread = threading.Thread(target=handle_client, args=(conn, addr))
            client_thread.start()
    finally:
        print(f'Closing TCP server...')
        s.close()


def monitor_input():
    global stop_server
    while True:
        cmd = input()
        if cmd == "/exit":
            print("Shutdown command received. Shutting down servers...")
            stop_server = True
            break


def monitor_clients(last_seen, client_files):
    while not stop_server:
        time.sleep(5)
        current_time = time.time()
        disconnected_clients = [addr for addr, last_time in last_seen.items() if
                                current_time - last_time > client_timeout]
        for addr in disconnected_clients:
            print(f"Client {addr[1]} is shutting down")
            client_files[addr].close()
            del client_files[addr]
            del last_seen[addr]


def main():
    tcp_thread = threading.Thread(target=start_tcp_server)
    udp_thread = threading.Thread(target=start_udp_server)
    input_thread = threading.Thread(target=monitor_input)

    tcp_thread.start()
    udp_thread.start()
    input_thread.start()

    udp_thread.join()
    tcp_thread.join()
    input_thread.join()


if __name__ == "__main__":
    main()
