#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <ctime>
#include <map>
#include <algorithm>
#include <condition_variable>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>

#define CHANNELS 1
#define RATE 44100
#define CHUNK 1024
#define SERVER_PORT 8888
#define CLIENT_TIMEOUT 1

int global_udp_port = -1;
bool stop_server = false;

std::mutex mtx;
std::condition_variable cv;

struct sockaddr_in_less {
    bool operator()(const sockaddr_in &lhs, const sockaddr_in &rhs) const {
        return std::tie(lhs.sin_family, lhs.sin_addr.s_addr, lhs.sin_port) <
               std::tie(rhs.sin_family, rhs.sin_addr.s_addr, rhs.sin_port);
    }
};

void handle_client(int conn, sockaddr_in addr) {
    std::cout << "Connected to TCP-server by " << inet_ntoa(addr.sin_addr) << std::endl;
    std::string udp_port_str = std::to_string(global_udp_port);
    send(conn, udp_port_str.c_str(), udp_port_str.size(), 0);
    close(conn);
}

void write_wav_header(std::ofstream &file) {
    char header[44] = {
        'R', 'I', 'F', 'F',  // Chunk ID
        0, 0, 0, 0,            // Chunk size (to be filled later)
        'W', 'A', 'V', 'E',    // Format
        'f', 'm', 't', ' ',    // Subchunk1 ID
        static_cast<char>(16), 0, 0, 0,           // Subchunk1 size
        static_cast<char>(1), 0,                  // Audio format (PCM)
        static_cast<char>(1), 0,                  // Number of channels (1 for mono)
        static_cast<char>(68), static_cast<char>(172), 0, 0,         // Sample rate (44100 Hz)
        static_cast<char>(136), static_cast<char>(88), static_cast<char>(1), 0,         // Byte rate (Sample rate * Number of channels * Bits per sample / 8)
        static_cast<char>(2), 0,                  // Block align (Number of channels * Bits per sample / 8)
        static_cast<char>(16), 0,                 // Bits per sample (16-bit)
        'd', 'a', 't', 'a',    // Subchunk2 ID
        0, 0, 0, 0             // Subchunk2 size (to be filled later)
    };

    file.write(header, 44);
}

void record_data(sockaddr_in client_addr, char *data,
                 std::map<sockaddr_in, std::ofstream *, sockaddr_in_less> &file_dict,
                 std::map<sockaddr_in, std::time_t, sockaddr_in_less> &last_seen) {
    if (file_dict.find(client_addr) == file_dict.end()) {
        // Open the file in binary mode and write the WAV header
        std::ofstream *wf = new std::ofstream(
            "output_" + std::to_string(client_addr.sin_addr.s_addr) + "_" + std::to_string(client_addr.sin_port) +
            ".wav", std::ios::binary);
        if (!wf->is_open()) {
            std::cerr << "Failed to open WAV file for writing." << std::endl;
            return;
        }

        write_wav_header(*wf);

        file_dict[client_addr] = wf;
        std::cout << "File created for " << inet_ntoa(client_addr.sin_addr) << std::endl;
    }

    file_dict[client_addr]->write(data, CHUNK * CHANNELS * 2);
    last_seen[client_addr] = std::time(nullptr);
}



void start_udp_server() {
    int udp_server = socket(AF_INET, SOCK_DGRAM, 0);
    sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_addr.sin_port = 0;

    bind(udp_server, (struct sockaddr *) &server_addr, sizeof(server_addr));

    socklen_t addr_len = sizeof(server_addr);
    getsockname(udp_server, (struct sockaddr *) &server_addr, &addr_len);

    global_udp_port = ntohs(server_addr.sin_port);
    std::cout << "UDP server listening on port " << global_udp_port << std::endl;

    std::map<sockaddr_in, std::ofstream *, sockaddr_in_less> client_files;
    std::map<sockaddr_in, std::time_t, sockaddr_in_less> last_seen;

    while (!stop_server) {
        struct pollfd fds[1];
        fds[0].fd = udp_server;
        fds[0].events = POLLIN;

        // Wait for activity with a timeout
        int ret = poll(fds, 1, 1000); // 1 second timeout
        if (ret == -1) {
            perror("poll");
            break;
        } else if (ret == 0) {
            continue; // Timeout
        }

        char data[CHUNK * CHANNELS * 2];
        sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        int bytes_received = recvfrom(udp_server, data, sizeof(data), 0, (struct sockaddr *) &client_addr, &client_len);
        if (bytes_received > 0) {
            record_data(client_addr, data, client_files, last_seen);
            std::cout << "Received " << bytes_received << " bytes of data from " << inet_ntoa(client_addr.sin_addr) <<
                    std::endl;
        }
    }

    close(udp_server);
}

void start_tcp_server() {
    int tcp_server = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    server_addr.sin_port = htons(SERVER_PORT);

    bind(tcp_server, (struct sockaddr *) &server_addr, sizeof(server_addr));
    listen(tcp_server, 5);

    std::cout << "TCP server is listening..." << std::endl;

    while (!stop_server) {
        // Setup poll structure
        struct pollfd fds[1];
        fds[0].fd = tcp_server;
        fds[0].events = POLLIN;

        // Wait for activity with a timeout
        int ret = poll(fds, 1, 1000); // 1 second timeout
        if (ret == -1) {
            perror("poll");
            break;
        } else if (ret == 0) {
            continue; // Timeout
        }

        sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        int conn = accept(tcp_server, (struct sockaddr *) &client_addr, &client_len);
        if (conn != -1) {
            std::thread client_thread(handle_client, conn, client_addr);
            client_thread.detach();
        }
    }

    close(tcp_server);
}

void monitor_input() {
    std::string cmd;
    while (true) {
        std::cin >> cmd;
        if (cmd == "/exit") {
            std::cout << "Shutdown command received. Shutting down servers..." << std::endl;
            stop_server = true;
            cv.notify_all(); // Notify all waiting threads to exit
            break;
        }
    }
}

void monitor_clients(std::map<sockaddr_in, std::ofstream *, sockaddr_in_less> &client_files,
                     std::map<sockaddr_in, std::time_t, sockaddr_in_less> &last_seen) {
    while (true) {
        std::unique_lock<std::mutex> lock(mtx);
        cv.wait_for(lock, std::chrono::seconds(1));
        if (stop_server) {
            break;
        }
        std::time_t current_time = std::time(nullptr);
        std::vector<sockaddr_in> disconnected_clients;
        for (auto const &pair: last_seen) {
            if (current_time - pair.second > CLIENT_TIMEOUT) {
                disconnected_clients.push_back(pair.first);
            }
        }
        for (auto const &addr: disconnected_clients) {
            std::cout << "Client " << inet_ntoa(addr.sin_addr) << " is shutting down" << std::endl;
            delete client_files[addr];
            client_files.erase(addr);
            last_seen.erase(addr);
        }
    }
}

int main() {
    std::thread tcp_thread(start_tcp_server);
    std::thread udp_thread(start_udp_server);
    std::thread input_thread(monitor_input);

    tcp_thread.join();
    udp_thread.join();
    input_thread.join();

    return 0;
}
