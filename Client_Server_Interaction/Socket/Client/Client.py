import socket
import pyaudio

tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp_client.connect(('127.0.0.1', 8888))
udp_port = int(tcp_client.recv(1024).decode('utf-8'))

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024

print(f"Received UDP port from server: {udp_port}")

tcp_client.close()

audio = pyaudio.PyAudio()
stream = audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK)
udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_addr = ('127.0.0.1', udp_port)

try:
    while True:
        data = stream.read(CHUNK, exception_on_overflow=False)
        udp_client.sendto(data, server_addr)
except KeyboardInterrupt:
    print("Terminating...")
finally:
    stream.stop_stream()
    stream.close()
    audio.terminate()
    udp_client.close()
