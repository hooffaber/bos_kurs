import pyaudio

audio = pyaudio.PyAudio()

for i in range(audio.get_device_count()):
    dev = audio.get_device_info_by_index(i)
    print(f"Device {i}: {dev['name']}, Channels: {dev['maxInputChannels']}")

default_input = audio.get_default_input_device_info()
default_output = audio.get_default_output_device_info()
print(f"Default Input Device: {default_input['name']}")
print(f"Default Output Device: {default_output['name']}")

print('\n\n')

audio.terminate()

p = pyaudio.PyAudio()
for i in range(p.get_device_count()):
    info = p.get_device_info_by_index(i)
    print(f"Device {i}: {info['name']}, Channels: {info['maxInputChannels']}")
p.terminate()

